/* Port of some SuperCollider UGens and stuff
 * Copyright (c) 2020, Daniele Pozzi <danepozzi at gmail dot com>
 */

import("stdfaust.lib");

sc = library("supercollider.lib");

sr = 48000.0;

//phasor
phasor(length) = %(max(length,1)) ~_ +(1);

//lfnoise0
lfnoise0(freq) = no.noise : ba.latch(os.osc(freq));

//lpf
lpf(sig, freq) = sig : fi.lowpass(2,freq) : _;

//compander
compress(in, cont, thresh, slopeBelow, slopeAbove, clampTime, relaxTime) = in * next_gain(doControl(relaxTime, clampTime, in), thresh, slopeBelow, slopeAbove)'
with{
    next_gain(val, thresh, below, above) = ba.if(
    val < thresh,
    ba.if(
        below == 1.0,
        1.0,
        pow(val / thresh, below - 1.0) + 0.00000111
    ),
    ba.if(
        above == 1.0,
        1.0,
        pow(val / thresh, above - 1.0) + 0.00000111
    )
    );

    doControl(relax, clamp, ctrl) = cntrol(relax, clamp) ~+(0)
    with{
	cntrol(relax, clamp, old, ctrl) = ba.if(
    	    abs(ctrl) < old, 
    	    abs(ctrl) + ((old - abs(ctrl)) * exp(log(0.1) / (relax * sr))), 
    	    abs(ctrl) + ((old - abs(ctrl)) * exp(log(0.1) / (clamp * sr)))
	    );
    };
};

//compander(in, cont, thresh, slopeBelow, slopeAbove, clampTime, relaxTime) = compress(in, abs(cont), thresh, slopeBelow, slopeAbove, clampTime, relaxTime);

comp(in, thresh, slopeBelow, slopeAbove, clampTime, relaxTime) = in <: compress(in, abs(in), thresh, slopeBelow, slopeAbove, clampTime, relaxTime);

//delayN
delayN(sig, length, del) = rwtable(sLength, 0.0, write, sig, read)
with{
    sLength = int(5*sr);
    write = int(phasor(sLength));
    read = int((sLength+phasor(sLength)-(del*sr))%sLength);
};

//cubic interpolation http://msp.ucsd.edu/techniques/latest/book-html/node114.html
cubic(sig) = sig <: (-sig + 9*sig@1 + 9*sig@2 - sig@3) / 16;

//fractional part
fracp(del) = samples - offset
with{
  samples = sr * del;
  offset = int(samples);
};

//cubicinterp()
//4-point, 3rd-order Hermite
//https://github.com/supercollider/supercollider/blob/develop/include/plugin_interface/SC_SndBuf.h#L217-L225
cubicinterp(x, sig) = sig <: ((c3 * x + c2) * x + c1) * x + c0
with{
  c0 = y1;
  c1 = 0.5 * (y2 - y0);
  c2 = y0 - 2.5 * y1 + 2.0 * y2 - 0.5 * y3;
  c3 = 0.5 * (y3 - y0) + 1.5 * (y1 - y2);

  y0 = sig; y1 = sig@1; y2 = sig@2; y3 = sig@3;
};

delayC(sig, length, del) = delayN(sig, length, del) : cubicinterp(fracp(del), _);

//reverse an array of channels
reverse(n) = route(n, n, par(i, n, (n-i, i+1)));

//interleave two consecutive arrays of channels
interleave(n) = route(n, n, par(i, n, (i+1, (i*2+1)%(n) + (i>(n/2-1)))));