x = SynthDef(\fdn,{
	arg dim = 0.0001935, slope = 0.1;
	var staircase = LeakDC.ar(SoundIn.ar(0))*3;
	var stairrms = staircase.abs.lag(0.05);
	var env = Env([0, 1, 1, 0], [0, 0.0026, 0]);
	var local = LocalIn.ar(16) + Impulse.ar(0);// + (staircase * EnvGen.kr(env));
	var decay = 9.90897;//MouseX.kr(0,9.90897);
	var rms = local.abs.lag(slope);//+(stairrms*2).poll);
	var loc = local+local.reverse;//+staircase;
	var room = dim+(FaustZc.ar(rms*loc, K2A.ar(rms*4680), K2A.ar(4680)*rms));
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /18;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh)*rms/2;
	Out.ar(0, walsh.copyRange(0,7));
	Out.ar(0, walsh.copyRange(8,15));
	Out.ar(8, rms);
}).add;


y = SynthDef(\zc,{
	arg pos = 4680, dur = 4680;
	var fdn = SoundIn.ar((0..7));
	var rms = SoundIn.ar((8..24));
	//provare a laggare ancora di piu rms
	var zc = FaustZc.ar(
		fdn,
		K2A.ar(1.465257*pos)*(rms*10),
		K2A.ar(1.565741*dur*rms)
	);
	var out = zc.copyRange(0,7) + zc.copyRange(8,15) + fdn*rms/2*5*6.dbamp;
	//var out = fdn*rms/2*5;

	Out.ar(0, out);
	//Out.ar(0, out[0]+out[1]+out[2]+out[3]);
	//Out.ar(7, out[4]+out[5]+out[6]+out[7]);
}).add;


x = Synth(\fdn, [\dim, 0.0001935, \slope, 60]); y = Synth(\zc, [\pos, 468, \dur, 400680], t); //minimal activity
x = Synth(\fdn, [\dim, 0.0001935, \slope, 0.1]); y = Synth(\zc, [\pos, 468, \dur, 4000680], t); //ritmo
x = Synth(\fdn, [\dim, 0.0001935, \slope, 0.1]); y = Synth(\zc, [\pos, 4680, \dur, 4680], t); //base
x = Synth(\fdn, [\dim, 3.0001935, \slope, 0.1]); y = Synth(\zc, [\pos, 4680, \dur, 4680], t); //ritmo
x = Synth(\fdn, [\dim, 2.0001935, \slope, 0.1]); y = Synth(\zc, [\pos, 4680, \dur, 400680], t); //ritmo
x = Synth(\fdn, [\dim, 0.1001935, \slope, 0.1]); y = Synth(\zc, [\pos, 4680, \dur, 4000680], t); //bello
x = Synth(\fdn, [\dim, 0.1001935, \slope, 10]); y = Synth(\zc, [\pos, 468, \dur, 40680], t); //armonico
x = Synth(\fdn, [\dim, 0.5001935, \slope, 20]); y = Synth(\zc, [\pos, 468, \dur, 400680], t); //armonico
x = Synth(\fdn, [\dim, 0.8001935, \slope, 5]); y = Synth(\zc, [\pos, 4068, \dur, 4068], t); //armonico
x = Synth(\fdn, [\dim, 0.8001935, \slope, 0.05]); y = Synth(\zc, [\pos, 4068, \dur, 4068], t); //armonico

z = SynthDef(\silence,{
	var in      = InFeedback.ar(0, 8);
	var sig     = Mix.new(in);
	var silence = DetectSilence.ar(sig, -38.0.dbamp, 1.5);
	silence = silence * -1 + 1;
	SendReply.kr(A2K.kr(silence - 0.5), '/silence', silence);
	SendReply.kr(A2K.kr(silence * -1 + 0.5), '/sound', silence);
}).play(t);