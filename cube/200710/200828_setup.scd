//add these lines inside prOnServerProcessExit method in
//usr/hshare/SuperCollider/SCClassLibrary/Common/Control/Server.sc
//to make the script recover in case of server crash
//"BYE BYE".postln;
//("/servergone sent to ").post ++ NetAddr.langPort.postln;
//NetAddr("localhost", NetAddr.langPort).sendMsg("/servergone", "byebye");


Window.closeAll;

~reset = {
	~jack_connect = "/usr/bin/jack_connect";
	~jack_disconnect = "/usr/bin/jack_disconnect";
	~connect = { arg port1, port2; (~jack_connect + port1 + port2).systemCmd };
	~disconnect = { arg port1, port2; (~jack_disconnect + port1 + port2).systemCmd };
	"killall -9 scsynth".unixCmd;
};

~server = {
	arg numIn = 24, numOut = 8, blockSize = 256, address = 58101;

	var name = ("local"++blockSize);
	var o = ServerOptions.new;
	o.numOutputBusChannels_(numOut)
	.numInputBusChannels_(numIn)
	.blockSize_(256)
	.hardwareBufferSize_(64)
	.numAudioBusChannels_(1024)
	.numControlBusChannels_(4096)
	.maxNodes_(2048)
	.memSize_(8192 * 128)
	.numWireBufs_(1024)
	.numBuffers_(2048)
	.sampleRate_(48000)
	.inDevice_("blocksize64")
	.outDevice_("blocksize64")
	;

	Server(name, NetAddr("127.0.0.1", address), o).makeWindow.boot;
};

~local = {
	arg numIn = 2, numOut = 24;

	s.options
	.numOutputBusChannels_(numOut)
	.numInputBusChannels_(numIn)
	.blockSize_(2)
	.hardwareBufferSize_(64)
	.numAudioBusChannels_(1024)
	.numControlBusChannels_(4096)
	.maxNodes_(2048)
	.memSize_(8192 * 128)
	.numWireBufs_(1024)
	.numBuffers_(2048)
	.sampleRate_(48000)
	.inDevice_("blocksize2")
	.outDevice_("blocksize2")
	;
	s.boot;
};

~instantiate = {
	{
		//fork(
		//t = ~server.(6,4,64,58101);
		//u = ~server.(10,10,128,58102), AppClock);
		//fork(t = ~server.(24,8,64,58101), AppClock);
		t = ~server.(24,8,64,58101);
	}.defer;
};

~connections = {

	(1..24).do{ arg i;
		~connect.("blocksize2:out_" ++ i, "blocksize64:in_" ++ i);
	};

	(1..8).do{ arg i;
		~connect.("blocksize64:out_" ++ i, "system:playback_" ++ i);
	};

	(1..2).do{ arg i;
		~connect.("system:capture_" ++ i, "blocksize2:in_" ++ i);
		~connect.("system:capture_" ++ i, "blocksize64:in_" ++ i);
	};
};

~disconnections = {

	(1..8).do{ arg i;
		~disconnect.("blocksize2:out_" ++ i, "system:playback_" ++ i);
	};

	(1..2).do{ arg i;
		~disconnect.("system:capture_" ++ i, "blocksize64:in_" ++ i);
	};
};

~start = {
	{
		5.wait;
		~reset.();
		5.wait;
		~local.();
		5.wait;
		~instantiate.();
		5.wait;
		~connections.();
		("connecting blocksize2 to blocksize64").postln;
		("connecting blocksize64 to system playback").postln;
		("connecting system caputre to blocksize2 and blocksize64").postln;
		5.wait;
		~disconnections.();
		("disconnecting blocksize2 from system playback").postln;
		2.wait;
		(thisProcess.nowExecutingPath.dirname +/+ "200827_osc.scd").load;
		("loading OSC functions").postln;
		2.wait;
		(thisProcess.nowExecutingPath.dirname +/+ "200829_defs.scd").load;
		("client active at ").post ++ NetAddr.langPort.postln;
		("starting sound").postln;
		~dead = true; //true if one server crashes, false if the second is killed at reboot
	}.fork;
};

~recover = {
	{
		~connections.();
	}.fork;
};

~walsh = {
	arg a = [1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0];
	var h = 1;
	var endVal = a.size - 1;
	while ({h < endVal}) {
		forBy (0, endVal, h*2) {
			arg i;
			for (i, (i+h-1)) {
				arg j;
				var x = a[j];
				var y = a[j+h];
				a[j] = x + y;
				a[j+h] = x - y;
			};
		};
		h = h*2;
	};
	a
};

~start.();
