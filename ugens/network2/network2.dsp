import("stdfaust.lib");

delayN(sig, del) = rwtable(sLength, 0.0, write, sig, read)
with{
    sLength = int(sr);
    write = int(phasor(sLength));
    read = int((sLength+phasor(sLength)-(del*sr))%sLength);
};

fracp(del) = samples - offset
with{
  samples = sr * del;
  offset = int(samples);
};

cubicinterp(x, sig) = sig <: ((c3 * x + c2) * x + c1) * x + c0
with{
  c0 = y1;
  c1 = 0.5 * (y2 - y0);
  c2 = y0 - 2.5 * y1 + 2.0 * y2 - 0.5 * y3;
  c3 = 0.5 * (y3 - y0) + 1.5 * (y1 - y2);

  y0 = sig; y1 = sig@1; y2 = sig@2; y3 = sig@3;
};

delayC(sig, del) = delayN(sig, del) : cubicinterp(fracp(del), _);

compress(in, cont, thresh, slopeBelow, slopeAbove, clampTime, relaxTime) = in * next_gain(doControl(relaxTime, clampTime, in), thresh, slopeBelow, slopeAbove)'
with{
    next_gain(val, thresh, below, above) = ba.if(
    val < thresh,
    ba.if(
        below == 1.0,
        1.0,
        pow(val / thresh, below - 1.0) + 0.00000111
    ),
    ba.if(
        above == 1.0,
        1.0,
        pow(val / thresh, above - 1.0) + 0.00000111
    )
    );

    doControl(relax, clamp, ctrl) = cntrol(relax, clamp) ~+(0)
    with{
	cntrol(relax, clamp, old, ctrl) = ba.if(
    	    abs(ctrl) < old, 
    	    abs(ctrl) + ((old - abs(ctrl)) * exp(log(0.1) / (relax * sr))), 
    	    abs(ctrl) + ((old - abs(ctrl)) * exp(log(0.1) / (clamp * sr)))
	    );
    };
};

comp(in, thresh, slopeBelow, slopeAbove, clampTime, relaxTime) = in <: compress(in, abs(in), thresh, slopeBelow, slopeAbove, clampTime, relaxTime);

sr = 48000;
dirac = 1-1';
r = 0.735;
t = 1000;
n = 16;
decay = 211.9 * 0.95;
primes = 2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53;

phasor(length) = %(max(length,1)) ~_ +(1);

time(n) = par(i, n, pow(_, r) / t);
delays(n) = par(i, n, delayC(_,_));
decays(n) = par(i, n, _ * 211.9 * 0.95);
lpfs(n) = par(i, n, fi.lowpass(2, 19000));

comps(n) = par(i, n, comp(_, 0.47, below, above, clamp, relax))
with{
    below = 1.0;
    above = 0.00000002;
    clamp = 0.0000015;
    relax = 3.9;
};

tanh(n) = par(i, n, ma.tanh(_));
old(n) = par(i, n, _ + dirac);
odd(n) = par(i, n, _);

cable(n) = par(i, n, _);
reverse(n) = route(n, n, par(i, n, (n-i, i+1)));
interleave(n) = route(n, n, par(i, n, (i+1, (i*2+1)%(n) + (i>(n/2-1)))));
sums(n) = par(i, n, +);

proc = old(n) <: cable(n), reverse(n) : interleave(n*2) :> sums(n), (primes : time(n)) : interleave(n*2) : delays(n) : decays(n) : ro.hadamard(n) : comps(n) : tanh(n) : lpfs(n);

process = odd(n) ~ proc;
