FaustNetwork2 : MultiOutUGen
{
  *ar {
      ^this.multiNew('audio')
  }

  *kr {
      ^this.multiNew('control')
  } 

  init { | ... theInputs |
      inputs = theInputs
      ^this.initOutputs(16, rate)
  }

  name { ^"FaustNetwork2" }


  info { ^"Generated with Faust" }
}

