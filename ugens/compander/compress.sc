FaustScCompander : UGen
{
  *ar { | in1, in2, in3, in4, in5, in6, in7 |
      ^this.multiNew('audio', in1, in2, in3, in4, in5, in6, in7)
  }

  *kr { | in1, in2, in3, in4, in5, in6, in7 |
      ^this.multiNew('control', in1, in2, in3, in4, in5, in6, in7)
  } 

  checkInputs {
    if (rate == 'audio', {
      7.do({|i|
        if (inputs.at(i).rate != 'audio', {
          ^(" input at index " + i + "(" + inputs.at(i) + 
            ") is not audio rate");
        });
      });
    });
    ^this.checkValidInputs
  }

  name { ^"FaustScCompander" }


  info { ^"Generated with Faust" }
}

