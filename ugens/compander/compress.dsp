declare name "sc-compander";
declare author "Daniele Pozzi";
declare ref "https://github.com/supercollider/supercollider/blob/develop/server/plugins/FilterUGens.cpp";

import("stdfaust.lib");

cntrol(ctrl, relax, clamp, old) = ba.if(
    ctrl < old, 
    ctrl + ((old - ctrl) * exp(log(0.1) / (relax * 48000.0))), 
    ctrl + ((old - ctrl) * exp(log(0.1) / (clamp * 48000.0)))
);

old = _;

doControl(ctrl, relax, clamp) = old ~ cntrol(ctrl, relax, clamp);

next_gain(val, thresh, below, above) = ba.if(
    val < thresh,
    ba.if(
        below == 1.0,
        1.0,
        pow(val / thresh, below - 1.0) + 0.00000111
    ),
    ba.if(
        above == 1.0,
        1.0,
        pow(val / thresh, above - 1.0) + 0.00000111
    )
);

compress(in, cont, thresh, slopeBelow, slopeAbove, clampTime, relaxTime) = in * next_gain(doControl(cont, relaxTime, clampTime), thresh, slopeBelow, slopeAbove)';
//taking the one-sample-old value of next_gain, as in original ugen:
//float gain_slope = CALCSLOPE(next_gain, gain);
//LOOP1(inNumSamples, ZXP(out) = ZXP(in) * gain; gain += gain_slope;);
//unit->m_gain = gain;

process(in, cont, thresh, slopeBelow, slopeAbove, clampTime, relaxTime) = compress(in, abs(cont), thresh, slopeBelow, slopeAbove, clampTime, relaxTime);
