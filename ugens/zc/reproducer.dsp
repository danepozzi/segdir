import("stdfaust.lib");

phasor(length) = %(length) ~_ +(1); //definition of a phasor. can be used to read/write into a table

table(read) = rwtable(tableLength,0.0,writeIndex,content,readIndex)
with{
    tableLength = 48000;
    writeIndex = phasor(tableLength);
    content = no.noise;
    readIndex = int(read);
};

//process = table(phasor(20)); //this works as expected
process = table(5.0); //trying to index with a constant doesnt return the item, but 0.0
//process = table(max(no.noise, 5)); //workaround: gets indexed even if value is always 5
//process = table(max(2, 5)); //this doesnt work: optimization?
