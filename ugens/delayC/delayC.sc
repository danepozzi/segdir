FaustDelayC : UGen
{
  *ar { | in1, in2, in3 |
      ^this.multiNew('audio', in1, in2, in3)
  }

  *kr { | in1, in2, in3 |
      ^this.multiNew('control', in1, in2, in3)
  } 

  checkInputs {
    if (rate == 'audio', {
      3.do({|i|
        if (inputs.at(i).rate != 'audio', {
          ^(" input at index " + i + "(" + inputs.at(i) + 
            ") is not audio rate");
        });
      });
    });
    ^this.checkValidInputs
  }

  name { ^"FaustDelayC" }


  info { ^"Generated with Faust" }
}

