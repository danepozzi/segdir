import("stdfaust.lib");

sr = 48000;
phasor(length) = %(max(length,1)) ~_ +(1);

delayN(sig, length, del) = rwtable(sLength, 0.0, write, sig, read)
with{
    sLength = int(5*sr);
    write = int(phasor(sLength));
    read = int((sLength+phasor(sLength)-(del*sr))%sLength);
};


process(sig, length, del) = delayN(sig, length, del);
