import("stdfaust.lib");

sr = 48000; 
phasor(length) = %(max(length,1)) ~_ +(1);

delayN(sig, length, del) = rwtable(sLength, 0.0, write, sig, read)
with{
    sLength = int(5*sr);
    write = int(phasor(sLength));
    read = int((sLength+phasor(sLength)-(del*sr))%sLength);
};

cubic(sig) = sig <: (-sig + 9*sig@1 + 9*sig@2 - sig@3) / 16;

fracp(del) = samples - offset
with{
  samples = sr * del;
  offset = int(samples);
};

hermite(x, sig) = sig <: ((c3 * x + c2) * x + c1) * x + c0
with{
  c0 = y1;
  c1 = 0.5 * (y2 - y0);
  c2 = y0 - 2.5 * y1 + 2.0 * y2 - 0.5 * y3;
  c3 = 0.5 * (y3 - y0) + 1.5 * (y1 - y2);

  y0 = sig; y1 = sig@1; y2 = sig@2; y3 = sig@3;
};

process(sig, length, del) = delayN(sig, length, del) : hermite(fracp(del), _);
