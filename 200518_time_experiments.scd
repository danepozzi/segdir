~walsh= {
	arg a = [1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0];
	var h = 1;
	var endVal = a.size - 1;
	while ({h < endVal}) {
		forBy (0, endVal, h*2) {
			arg i;
			for (i, (i+h-1)) {
				arg j;
				var x = a[j];
				var y = a[j+h];
				a[j] = x + y;
				a[j+h] = x - y;
			};
		};
		h = h*2;
	};
	a
}

////200519

SynthDef(\walsh,{
	var decay = MouseY.kr(0.029, 12.19);
	var local = LocalIn.ar(16) + Impulse.ar(0) + SoundIn.ar(1);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.735+FaustZc.ar(rms*loc, K2A.ar(MouseX.kr(0,1)*46680), K2A.ar(46680));
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /500;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9);
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(MouseX.kr(0,1)*46680)*rms, K2A.ar(MouseY.kr(0,1)*46680*rms)))*rms/2);
}).play;

s.record

SynthDef(\walsh,{
	var decay = MouseY.kr(0.029, 12.19);
	var local = LocalIn.ar(16) + Impulse.ar(0) + SoundIn.ar(1);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.735+FaustZc.ar(rms*loc, K2A.ar(MouseX.kr(0,1)*46680), K2A.ar(46680));
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /500;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(MouseX.kr(0,1)*46680)*(rms*10), K2A.ar(MouseY.kr(0,1)*46680*rms)))*rms/2);
}).play;



SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0) + SoundIn.ar(1);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.735+FaustZc.ar(rms*loc, K2A.ar(0.465257*46680), K2A.ar(46680));
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /500;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(0.465257*46680)*(rms*10), K2A.ar(0.565741*46680*rms)))*rms/2);
}).play;




SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0) + SoundIn.ar(1);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.735+FaustZc.ar(rms*loc, K2A.ar(0.465257*46680), K2A.ar(46680));
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1500;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(0.465257*46680)*(rms*10), K2A.ar(0.565741*46680*rms)))*rms/2);
}).play;




SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0) + SoundIn.ar(1);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.735+FaustZc.ar(rms*loc, K2A.ar(0.465257*46680)+rms, K2A.ar(46680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1500;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(0.465257*46680)*(rms*10), K2A.ar(0.565741*46680*rms)))*rms/2);
}).play;




SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0) + SoundIn.ar(1);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.8935+FaustZc.ar(rms*loc, K2A.ar(0.465257*46680), K2A.ar(46680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1500;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(0.465257*46680)*(rms*10), K2A.ar(0.565741*46680*rms)))*rms/2);
}).play;


// converging
SynthDef(\walsh,{
	var decay = 0.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0) + SoundIn.ar(1);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.8935+FaustZc.ar(rms*loc, K2A.ar(0.465257*46680), K2A.ar(46680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1500;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(0.465257*46680)*(rms*10), K2A.ar(0.565741*46680*rms)))*rms/2);
}).play;




SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.8935+FaustZc.ar(rms*loc, K2A.ar(0.465257*46680), K2A.ar(46680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /600;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(0.465257*46680)*(rms*10), K2A.ar(0.565741*46680*rms)))*rms/2);
}).play;


//
SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.8935+FaustZc.ar(rms*loc, K2A.ar(0.465257*46680), K2A.ar(46680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1600;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(0.465257*46680)*(rms*10), K2A.ar(0.565741*46680*rms)))*rms/2);
}).play;


//SC_200518_235434.aiff
SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.8935+FaustZc.ar(rms*loc, K2A.ar(0.465257*4680), K2A.ar(4680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /2600;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(0.465257*46680)*(rms*10), K2A.ar(0.565741*46680*rms)))*rms/2);
}).play;



SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.8935+FaustZc.ar(rms*loc, K2A.ar(rms*46800), K2A.ar(4680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /2600;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(0.465257*46680)*(rms*10), K2A.ar(0.565741*46680*rms)))*rms/2);
}).play;





SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.01);
	var loc = local+local.reverse;
	var room = 0.8935+FaustZc.ar(rms*loc, K2A.ar(rms*46800), K2A.ar(4680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /300;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(0.465257*46680)*(rms*10), K2A.ar(0.565741*46680*rms)))*rms/2);
}).play;



////SC_200519_003256.aiff
SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.1);
	var loc = local+local.reverse;
	var room = 0.8935+FaustZc.ar(rms*loc, K2A.ar(rms*46800), K2A.ar(4680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1800;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(1.465257*46680)*(rms*10), K2A.ar(1.565741*46680*rms)))*rms/2);
}).play;



////200519


//1m 0954
SynthDef(\walsh,{
	var decay = 6.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.1);
	var loc = local+local.reverse;
	var room = 0.4935+FaustZc.ar(rms*loc, K2A.ar(rms*4680), K2A.ar(4680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1800;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(1.465257*46680)*(rms*10), K2A.ar(1.565741*46680*rms)))*rms/2);
}).play;




//3m 10.00
SynthDef(\walsh,{
	var decay = 9.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.1);
	var loc = local+local.reverse;
	var room = 0.1935+FaustZc.ar(rms*loc, K2A.ar(rms*4680), K2A.ar(4680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1800;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(1.465257*46680)*(rms*10), K2A.ar(1.565741*46680*rms)))*rms/2);
}).play;


//4m 10.06
SynthDef(\walsh,{
	var decay = 9.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.1);
	var loc = local+local.reverse;
	var room = 0.01935+FaustZc.ar(rms*loc, K2A.ar(rms*4680), K2A.ar(4680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1800;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(1.465257*46680)*(rms*10), K2A.ar(1.565741*46680*rms)))*rms/2);
}).play;



//4m 10.09
SynthDef(\walsh,{
	var decay = 9.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.1);
	var loc = local+local.reverse;
	var room = 0.001935+FaustZc.ar(rms*loc, K2A.ar(rms*4680), K2A.ar(4680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1800;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(1.465257*46680)*(rms*10), K2A.ar(1.565741*46680*rms)))*rms/2);
}).play;


//SC_200519_101659.aiff
//from click to tone, then dies
SynthDef(\walsh,{
	var decay = 9.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.1);
	var loc = local+local.reverse;
	var room = 0.0001935+FaustZc.ar(rms*loc, K2A.ar(rms*4680), K2A.ar(4680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /1800;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(1.465257*4680)*(rms*10), K2A.ar(1.565741*4680*rms)))*rms/2);
}).play;


//stretched SC_200519_102510.aiff
SynthDef(\walsh,{
	var decay = 9.90897;
	var local = LocalIn.ar(16) + Impulse.ar(0);
	var rms = local.abs.lag(0.1);
	var loc = local+local.reverse;
	var room = 0.0001935+FaustZc.ar(rms*loc, K2A.ar(rms*4680), K2A.ar(4680)*rms);
	var lengths = [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53]** room /18;
	var delay = DelayL.ar(loc, 2, lengths);
	var walsh = delay;
	walsh = walsh * (decay* (1-rms.lag(10)));
	walsh = ~walsh.value(walsh);
	walsh = Compander.ar(walsh, walsh, 0.47, 1.0, LPF.ar(delay[1].abs,2)/1000, 0.00000015, 3.9+(rms*1000));
	walsh = walsh.tanh;
	LocalOut.ar(LPF.ar(walsh, 20000));
	walsh = LeakDC.ar(walsh);
	Out.ar([0,2], Splay.ar(walsh)+(FaustZc.ar(walsh[0], K2A.ar(1.465257*4680)*(rms*10), K2A.ar(1.565741*4680*rms)))*rms/2);
}).play;